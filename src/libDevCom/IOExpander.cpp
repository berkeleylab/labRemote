#include "IOExpander.h"

#include "Logger.h"

void IOExpander::setIO(uint32_t bit, bool input) {
    uint8_t value = getIO();
    if (input)
        value |= (1 << bit);
    else
        value &= ~(1 << bit);
    setIO(value);
}

void IOExpander::setInternalPullUp(uint32_t output) {
    logger(logWARNING)
        << "setInternalPullUp not implemented for this IOExpander.";
    return;
}

void IOExpander::write(uint32_t bit, bool value) {
    uint8_t oldvalue = read();
    if (value)
        oldvalue |= (1 << bit);
    else
        oldvalue &= ~(1 << bit);
    write(oldvalue);
}

bool IOExpander::read(uint32_t bit) {
    uint8_t value = read();
    return (value >> bit) & 1;
}
