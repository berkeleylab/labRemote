#include "PCA9548ACom.h"

#include "DeviceComRegistry.h"
#include "Logger.h"
REGISTER_DEVCOM(PCA9548A, I2CCom)

#include "ComIOException.h"
#include "ScopeLock.h"

PCA9548ACom::PCA9548ACom(uint8_t deviceAddr, uint8_t channel,
                         std::shared_ptr<I2CCom> com)
    : I2CCom(deviceAddr),
      m_com(com),
      m_muxAddr(com->deviceAddr()),
      m_channel(channel) {}

PCA9548ACom::~PCA9548ACom() {}

void PCA9548ACom::write_reg32(uint32_t address, uint32_t data) {
    ScopeLock lock(m_com);

    m_com->write_reg8(1 << m_channel);

    m_com->setDeviceAddr(deviceAddr());
    try {
        m_com->write_reg32(address, data);
        restore_com();
    } catch (const ComIOException& e) {
        restore_com();
        throw;
    }
}

void PCA9548ACom::write_reg16(uint32_t address, uint16_t data) {
    ScopeLock lock(m_com);

    m_com->write_reg8(1 << m_channel);

    m_com->setDeviceAddr(deviceAddr());
    try {
        m_com->write_reg16(address, data);
        restore_com();
    } catch (const ComIOException& e) {
        restore_com();
        throw;
    }
}

void PCA9548ACom::write_reg8(uint32_t address, uint8_t data) {
    ScopeLock lock(m_com);

    m_com->write_reg8(1 << m_channel);

    m_com->setDeviceAddr(deviceAddr());
    try {
        m_com->write_reg8(address, data);
        restore_com();
    } catch (const ComIOException& e) {
        restore_com();
        throw;
    }
}

void PCA9548ACom::write_reg32(uint32_t data) {
    ScopeLock lock(m_com);

    m_com->write_reg8(1 << m_channel);

    m_com->setDeviceAddr(deviceAddr());
    try {
        m_com->write_reg32(data);
        restore_com();
    } catch (const ComIOException& e) {
        restore_com();
        throw;
    }
}

void PCA9548ACom::write_reg16(uint16_t data) {
    ScopeLock lock(m_com);

    m_com->write_reg8(1 << m_channel);

    m_com->setDeviceAddr(deviceAddr());
    try {
        m_com->write_reg16(data);
        restore_com();
    } catch (const ComIOException& e) {
        restore_com();
        throw;
    }
}

void PCA9548ACom::write_reg8(uint8_t data) {
    ScopeLock lock(m_com);

    m_com->write_reg8(1 << m_channel);

    m_com->setDeviceAddr(deviceAddr());
    try {
        m_com->write_reg8(data);
        restore_com();
    } catch (const ComIOException& e) {
        restore_com();
        throw;
    }
}

void PCA9548ACom::write_block(uint32_t address,
                              const std::vector<uint8_t>& data) {
    ScopeLock lock(m_com);

    m_com->write_reg8(1 << m_channel);

    m_com->setDeviceAddr(deviceAddr());
    try {
        m_com->write_block(address, data);
        restore_com();
    } catch (const ComIOException& e) {
        restore_com();
        throw;
    }
}

void PCA9548ACom::write_block(const std::vector<uint8_t>& data) {
    ScopeLock lock(m_com);

    m_com->write_reg8(1 << m_channel);

    m_com->setDeviceAddr(deviceAddr());
    try {
        m_com->write_block(data);
        restore_com();
    } catch (const ComIOException& e) {
        restore_com();
        throw;
    }
}

uint32_t PCA9548ACom::read_reg32(uint32_t address) {
    ScopeLock lock(m_com);

    m_com->write_reg8(1 << m_channel);

    m_com->setDeviceAddr(deviceAddr());
    uint32_t data = 0;
    try {
        data = m_com->read_reg32(address);
        restore_com();
    } catch (const ComIOException& e) {
        restore_com();
        throw;
    }
    return data;
}

uint32_t PCA9548ACom::read_reg24(uint32_t address) {
    ScopeLock lock(m_com);

    m_com->write_reg8(1 << m_channel);

    m_com->setDeviceAddr(deviceAddr());
    uint32_t data = 0;
    try {
        data = m_com->read_reg24(address);
        restore_com();
    } catch (const ComIOException& e) {
        restore_com();
        throw;
    }
    return data;
}

uint16_t PCA9548ACom::read_reg16(uint32_t address) {
    ScopeLock lock(m_com);

    m_com->write_reg8(1 << m_channel);

    m_com->setDeviceAddr(deviceAddr());
    uint16_t data = 0;
    try {
        data = m_com->read_reg16(address);
        restore_com();
    } catch (const ComIOException& e) {
        restore_com();
        throw;
    }
    return data;
}

uint8_t PCA9548ACom::read_reg8(uint32_t address) {
    ScopeLock lock(m_com);

    m_com->write_reg8(1 << m_channel);

    m_com->setDeviceAddr(deviceAddr());
    uint8_t data = 0;
    try {
        data = m_com->read_reg8(address);
        restore_com();
    } catch (const ComIOException& e) {
        restore_com();
        throw;
    }
    return data;
}

uint32_t PCA9548ACom::read_reg32() {
    ScopeLock lock(m_com);

    m_com->write_reg8(1 << m_channel);

    m_com->setDeviceAddr(deviceAddr());
    uint32_t data = 0;
    try {
        data = m_com->read_reg32();
        restore_com();
    } catch (const ComIOException& e) {
        restore_com();
        throw;
    }
    return data;
}

uint32_t PCA9548ACom::read_reg24() {
    ScopeLock lock(m_com);

    m_com->write_reg8(1 << m_channel);

    m_com->setDeviceAddr(deviceAddr());
    uint32_t data = 0;
    try {
        data = m_com->read_reg24();
        restore_com();
    } catch (const ComIOException& e) {
        restore_com();
        throw;
    }
    return data;
}

uint16_t PCA9548ACom::read_reg16() {
    ScopeLock lock(m_com);

    m_com->write_reg8(1 << m_channel);

    m_com->setDeviceAddr(deviceAddr());
    uint16_t data = 0;
    try {
        data = m_com->read_reg16();
        restore_com();
    } catch (const ComIOException& e) {
        restore_com();
        throw;
    }
    return data;
}

uint8_t PCA9548ACom::read_reg8() {
    ScopeLock lock(m_com);

    m_com->write_reg8(1 << m_channel);

    m_com->setDeviceAddr(deviceAddr());
    uint8_t data = 0;
    try {
        data = m_com->read_reg8();
        restore_com();
    } catch (const ComIOException& e) {
        restore_com();
        throw;
    }
    return data;
}

void PCA9548ACom::read_block(uint32_t address, std::vector<uint8_t>& data) {
    ScopeLock lock(m_com);

    m_com->write_reg8(1 << m_channel);

    m_com->setDeviceAddr(deviceAddr());
    try {
        m_com->read_block(address, data);
        restore_com();
    } catch (const ComIOException& e) {
        restore_com();
        throw;
    }
}

void PCA9548ACom::read_block(std::vector<uint8_t>& data) {
    ScopeLock lock(m_com);

    m_com->write_reg8(1 << m_channel);

    m_com->setDeviceAddr(deviceAddr());
    try {
        m_com->read_block(data);
        restore_com();
    } catch (const ComIOException& e) {
        restore_com();
        throw;
    }
}

void PCA9548ACom::lock() {
    logger(logDEBUG4) << __PRETTY_FUNCTION__ << " -> PCA9548ACom locked!";
    m_com->lock();
}

void PCA9548ACom::unlock() {
    logger(logDEBUG4) << __PRETTY_FUNCTION__ << " -> PCA9548ACom unlocked!";
    m_com->unlock();
}

void PCA9548ACom::restore_com() {
    m_com->setDeviceAddr(m_muxAddr);
    m_com->write_reg8(0);
}
