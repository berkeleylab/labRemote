#ifndef ADS7828_H
#define ADS7828_H

#include <stdint.h>

#include <memory>

#include "ADCDevice.h"
#include "I2CCom.h"

// clang-format off
/** \brief ADS7828: 12-Bit, 8-Channel ADC with I2C
 *
 * [Datasheet](https://www.ti.com/lit/ds/symlink/ads7828.pdf).
 *
 * Channel number definition when using differential mode:
 * 0: CH0 vs CH1 (CH0 on +, CH1 on -) 
 * 1: CH1 vs CH0
 * 2: CH2 vs CH3
 * 3: CH3 vs CH2
 * 4: CH4 vs CH5
 * 5: CH5 vs CH4
 * 6: CH6 vs CH7
 * 7: CH7 vs CH6
 */
// clang-format on
class ADS7828 : public ADCDevice {
 public:
    ADS7828(double reference, std::shared_ptr<I2CCom> com);
    virtual ~ADS7828();

    virtual int32_t readCount();
    virtual int32_t readCount(uint8_t ch);
    virtual void readCount(const std::vector<uint8_t>& chs,
                           std::vector<int32_t>& data);

    //! \brief Allowed measurement modes: either differential or single-ended
    enum class ChannelMode { SingleEnded, Differential };

    //! \brief Set the channel mode
    void setChannelMode(ChannelMode mode);

    //! \brief Get the current channel mode
    ChannelMode getChannelMode() const { return m_channelMode; }

 private:
    // Properties of device
    ChannelMode m_channelMode = ChannelMode::SingleEnded;
    static const uint32_t m_maxValue;

    std::shared_ptr<I2CCom> m_com;
};

#endif  // ADS7828_H
