#include "ADS7828.h"

#include "DeviceComRegistry.h"
#include "LinearCalibration.h"
#include "OutOfRangeException.h"

REGISTER_DEVCOM(ADS7828, ADCDevice)

const uint32_t ADS7828::m_maxValue = 0xFFF;

ADS7828::ADS7828(double reference, std::shared_ptr<I2CCom> com)
    : ADCDevice(std::make_shared<LinearCalibration>(reference, m_maxValue)),
      m_com(com) {}

ADS7828::~ADS7828() {}

int32_t ADS7828::readCount() { return m_com->read_reg16() & m_maxValue; }

int32_t ADS7828::readCount(uint8_t ch) {
    uint8_t SD = 0;
    if (m_channelMode == ChannelMode::SingleEnded) SD = 1;
    uint8_t PD = 1;
    uint8_t C210 = 0;

    switch (ch) {
        case 0:
            C210 = 0b000;
            break;
        case 1:
            C210 = 0b100;
            break;
        case 2:
            C210 = 0b001;
            break;
        case 3:
            C210 = 0b101;
            break;
        case 4:
            C210 = 0b010;
            break;
        case 5:
            C210 = 0b110;
            break;
        case 6:
            C210 = 0b011;
            break;
        case 7:
            C210 = 0b111;
            break;
        default:
            throw OutOfRangeException(ch, 0, 7);
            break;
    };

    uint8_t command = (SD << 7) | (C210 << 4) | (PD << 2);

    m_com->write_reg8(command);

    return m_com->read_reg16() & m_maxValue;
}

void ADS7828::readCount(const std::vector<uint8_t>& chs,
                        std::vector<int32_t>& counts) {
    counts.clear();
    for (const uint8_t channel : chs) {
        counts.push_back(readCount(channel));
    }
}

void ADS7828::setChannelMode(ChannelMode mode) { m_channelMode = mode; }
