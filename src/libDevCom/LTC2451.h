#ifndef LTC2451_H
#define LTC2451_H

#include <stdint.h>

#include <memory>

#include "ADCDevice.h"
#include "I2CCom.h"

/** \brief LTC2451: Ultra-Tiny, 16-Bit ΔΣ ADC with I2C Interface
 *
 * [Datasheet](https://www.analog.com/en/products/ltc2451.html)
 */
class LTC2451 : public ADCDevice {
 public:
    enum Speed { Speed60Hz = 0, Speed30Hz = 1 };

    LTC2451(double reference, std::shared_ptr<I2CCom> com);
    virtual ~LTC2451();

    virtual int32_t readCount();
    virtual int32_t readCount(uint8_t ch);
    virtual void readCount(const std::vector<uint8_t>& chs,
                           std::vector<int32_t>& data);

    // LTC2451 functionality
    void setSpeed(Speed speed);

 private:
    std::shared_ptr<I2CCom> m_com;

    //! \brief wait until conversion finishes
    /**
     * Implemented as a 1/30Hz+15ms sleep to handle the
     * longest period a conversion can take.
     */
    void wait_for_finish();
};

#endif  // LTC2451_H
