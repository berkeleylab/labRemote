#include "Fluke45.h"

#include "IMeter.h"
#include "Logger.h"
#include "MeterRegistry.h"
#include "ScopeLock.h"
REGISTER_METER(Fluke45)

Fluke45::Fluke45(const std::string& name) : IMeter(name, {"Fluke45"}) {}

bool Fluke45::ping(unsigned dev) {
    std::string result = "";
    if (dev == 0) {
        logger(logDEBUG) << "ping the multimeter.....";
        result = m_com->sendreceive("*IDN?\n");
    } else {
        throw std::runtime_error("Other channels not implemented! ");
    }
    return !result.empty();
}

std::string Fluke45::identify() {
    std::string idn = this->sendreceive("*IDN?");
    return idn;
}

void Fluke45::reset() {
    logger(logDEBUG) << __PRETTY_FUNCTION__ << " -> Initialising: ";
    this->send("*RST");
}

std::string Fluke45::GetMode() { return this->sendreceive("FUNC1?"); }

void Fluke45::send(std::string cmd) {
    ScopeLock lock(m_com);
    m_com->send("*CLS");
    logger(logDEBUG2) << __PRETTY_FUNCTION__ << " -> Sending: " << cmd;
    cmd += "\r\n";
    m_com->send(cmd);
    std::this_thread::sleep_for(std::chrono::milliseconds(m_wait));
}

std::string Fluke45::sendreceive(std::string cmd) {
    ScopeLock lock(m_com);
    m_com->send("*CLS");
    logger(logDEBUG2) << __PRETTY_FUNCTION__ << " -> Sending: " << cmd;
    cmd += "\r\n";
    m_com->send(cmd);
    m_com->send("++read eoi\n\r");
    std::this_thread::sleep_for(std::chrono::milliseconds(m_wait));
    std::string buf = m_com->receive();
    logger(logDEBUG2) << __PRETTY_FUNCTION__ << " -> Received: " << buf;
    return buf;
}

std::string Fluke45::GetValue() { return this->sendreceive("VAL1?"); }

double Fluke45::measureDCV(unsigned channel) {
    ScopeLock lock(m_com);
    std::string CurrentMode = this->GetMode();
    if (CurrentMode != "VDC") {
        logger(logDEBUG2) << "Current Mode is " + CurrentMode +
                                 ", Reset the meter to VDC";
        this->reset();
        this->SetMode(Fluke45Mode::VOLTAGEDC);
    }

    if (channel > 0)  // use scanner card; not implemented for Fluke 45
        std::cerr << "Unimplemented channel number: " << channel << std::endl;

    return std::stod(this->GetValue());
}

double Fluke45::measureDCI(unsigned channel) {
    ScopeLock lock(m_com);
    std::string CurrentMode = this->GetMode();
    if (CurrentMode != "ADC") {
        logger(logDEBUG2) << "Current Mode is " + CurrentMode +
                                 ", Reset the meter to ADC";
        this->reset();
        this->SetMode(Fluke45Mode::CURRENTDC);
    }

    if (channel > 0)  // use scanner card; not implemented for Fluke 45
        std::cerr << "Unimplemented channel number: " << channel << std::endl;

    std::string val = this->GetValue();
    return std::stod(val);
}

double Fluke45::measureRES(unsigned channel, bool use4w) {
    ScopeLock lock(m_com);
    std::string CurrentMode = this->GetMode();
    if (CurrentMode != "OHMS") {
        logger(logDEBUG2) << "Current Mode is " + CurrentMode +
                                 ", Reset the meter to OHMS";
        this->reset();
        this->SetMode(Fluke45Mode::OHMS);
    }

    if (channel > 0)  // use scanner card; not implemented for Fluke 45
        std::cerr << "Unimplemented channel number: " << channel << std::endl;

    std::string val = this->GetValue();
    return std::stod(val);
}

double Fluke45::measureCAP(unsigned channel) {
    std::cerr << "Unable to measure capacitance by Fluke 45, exit. "
              << std::endl;
}

void Fluke45::SetMode(enum Fluke45Mode mode) {
    switch (mode) {
        case Fluke45Mode::VOLTAGEDC:
            this->send("VDC; AUTO;");
            break;
        case Fluke45Mode::VOLTAGEAC:
            this->send("VAC; AUTO;");
            break;
        case Fluke45Mode::CURRENTDC:
            this->send("ADC; AUTO;");
            break;
        case Fluke45Mode::CURRENTAC:
            this->send("AAC; AUTO;");
            break;
        case Fluke45Mode::OHMS:
            this->send("OHMS; AUTO;");
            break;
        default:
            logger(logERROR) << __PRETTY_FUNCTION__ << " : Unknown mode!";
            break;
    }
}

void Fluke45::checkCompatibilityList() {
    // get model connected to the meter
    std::string idn = this->identify();

    // get list of models
    std::vector<std::string> models = IMeter::getListOfModels();

    if (models.empty()) {
        logger(logINFO) << "No model identifier implemented for this meter. No "
                           "check is performed.";
        return;
    }

    std::size_t pos = m_name.find("Fluke");
    std::string brand = m_name.substr(pos, pos + 5);
    std::string type = m_name.substr(pos + 5, pos + 2);

    for (int i = 0; i < brand.length(); i++) {
        brand[i] = toupper(brand[i]);
    }

    for (const std::string& model : models) {
        if (idn.find(brand) != std::string::npos &&
            idn.find(type) != std::string::npos)
            return;
    }

    logger(logERROR) << "Unknown meter: " << idn;
    throw std::runtime_error("Unknown meter: " + idn);
}
