#ifndef Fluke8842_Hmultimeter
#define Fluke8842_H
#include <chrono>
#include <iostream>
#include <string>
#include <thread>

#include "IMeter.h"

/*
 Fluke8842A multimeter
 Author: Kehang Bai
 Date: August 2022
 Reference 1:
 Fluke 45 User Manual
 https://xdevs.com/doc/Fluke/8842A/FLUKE_8842A_Instruction.pdf
 Reference 2:
 GPIB usb controller
 https://prologix.biz/gpib-usb-controller.html

*/

enum class FlukeMode { VOLTAGEDC, CURRENTDC, OHMS, FOHMS };

class Fluke8842 : public IMeter {
 public:
    /** Constructor.
     * @param name: name of the meter
     */
    Fluke8842(const std::string& name);

    /**
     * @brief Reset the multimeter
     */
    void reset();

    /**
     * @brief ping the device
     *
     * @param dev index of the device to ping; default is 0.
     * @return true: the meter can be connected
     * @return false: the meter cannet be connected
     */
    bool ping(unsigned dev = 0);

    /**
     * @brief Get the identification of the Meter.
     *
     * @return std::string the identification code of the meter as four
     *                     fields separated by commas.
     */
    std::string identify();

    /**
     * @brief Get the current measurement mode of the meter.
     *
     * @return std::string Currently only four modes are returned: VDC, VAC,
     * ADC, AAC.
     */
    std::string GetMode();

    /**
     * @brief Get the value on the display 1
     *
     * @return std::string
     */
    std::string GetValue();

    /**
     * @brief Set the measurement mode, possible modes are VDC, VAC, ADC, AAC.
     *
     */
    void SetMode(enum FlukeMode);

    /**
     * @brief Measure the DC volt
     *
     * @param channel
     * @return double
     */
    double measureDCV(unsigned channel = 0);

    /**
     * @brief Measure the DC current
     *
     * @param channel
     * @return double
     */
    double measureDCI(unsigned channel = 0);

    /**
     * @brief Measure resistance
     *
     * @param channel
     * @return double
     */
    double measureRES(unsigned channel = 0, bool use4w = false);

    /** Fluke8842 cannot measure capacitance.
     * @brief Measure capacitance
     *
     * @param channel
     *
     */
    double measureCAP(unsigned channel = 0);

    /**
     * @brief Check that the device model is supported.
     * Overwrite the IMeter implement because idn(FLUKE, 45, 5355171, 1.7 D1.0)
     * is different from Fluke 45.
     *
     */
    void checkCompatibilityList();

 private:
    /**
     * @brief Send a command string to the meter.
     *
     * @param cmd
     */
    void send(std::string cmd);
    unsigned m_addr;
    /**
     * @brief Send a command string to the meter and read the meter output.
     *
     * @param cmd
     * @return std::string
     */
    std::string sendreceive(std::string cmd);
    /**
     * @brief wait interval. could be used between two successive measurement.
     *
     */
    std::chrono::milliseconds m_wait{900};
};

#endif
