#ifndef Fluke45_H
#define Fluke45_H
#include <chrono>
#include <iostream>
#include <string>
#include <thread>

#include "IMeter.h"

/*
 Fluke45 multimeter
 Author: Haoran Zhao & Emily Thompson
 Date: Feb 2023
 Reference 1:
 Fluke 45 User Manual
 https://www.testequipmentdepot.com/usedequipment/pdf/45.pdf
 Reference 2:
 GPIB usb controller
 https://prologix.biz/gpib-usb-controller.html
*/

enum class Fluke45Mode { VOLTAGEDC, VOLTAGEAC, CURRENTDC, CURRENTAC, OHMS };

class Fluke45 : public IMeter {
 public:
    // constructor
    Fluke45(const std::string& name);

    // Reset the multimeter
    void reset();

    // Ping device
    bool ping(unsigned dev = 0);

    // Ping device
    std::string identify();

    // Get the current measurement mode of the multimeter
    std::string GetMode();

    // Get value on the display
    std::string GetValue();

    // Set measurement mode (VDC, VAC, ADC, AAC)
    void SetMode(enum Fluke45Mode);

    // Make measurements
    double measureDCV(unsigned channel = 0);
    double measureDCI(unsigned channel = 0);
    double measureRES(unsigned channel = 0, bool use4w = false);
    double measureCAP(unsigned channel = 0);

    // Check if device model is supported
    void checkCompatibilityList();

 private:
    // Send command string to reader
    void send(std::string cmd);

    // Send command string to meter and read the output
    std::string sendreceive(std::string cmd);

    // Brief wait interval, could be used between two successive measurements
    std::chrono::milliseconds m_wait{900};
};

#endif
