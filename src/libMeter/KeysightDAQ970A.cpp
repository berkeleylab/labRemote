#include "KeysightDAQ970A.h"

#include <algorithm>  // std::transform
#include <chrono>
#include <sstream>  // std::stringstream
#include <thread>   // std::this_thread

#include "CharDeviceCom.h"
#include "Logger.h"
#include "MeterRegistry.h"
#include "ScopeLock.h"
#include "StringUtils.h"  // utils::rtrim
REGISTER_METER(KeysightDAQ970A)

const std::vector<unsigned> KeysightDAQ970A::m_validChannelsDCI = {
    121, 122, 221, 222, 321, 322};

KeysightDAQ970A::KeysightDAQ970A(const std::string& name)
    : IMeter(name, {"DAQ970A"}) {}

std::string KeysightDAQ970A::identify() {
    std::string id_string = sendreceive("*IDN?");
    utils::trim(id_string);
    return id_string;
}

bool KeysightDAQ970A::ping(unsigned /*dev*/) {
    std::string id = m_com->sendreceive("*IDN?");
    utils::trim(id);
    return !id.empty();
}

void KeysightDAQ970A::reset() {
    logger(logDEBUG) << __PRETTY_FUNCTION__ << " Sending reset command";
    send("*RST");
}

void KeysightDAQ970A::waitForOPC() {
    ScopeLock lock(m_com);
    std::string opc = m_com->sendreceive("*OPC?");
    utils::rtrim(opc);
    if (opc != "1")
        throw std::runtime_error(
            "KeysightDAQ970A command not completed (OPC reply: " + opc + ")");
}

void KeysightDAQ970A::waitForEventCompletion() {
    m_com->send("*OPC");
    int ESR = 0;
    do {
        ESR = std::stoi(m_com->sendreceive("*ESR?"));
        std::this_thread::sleep_for(m_wait);
    } while ((ESR & 1) == 0);
}

void KeysightDAQ970A::send(const std::string& cmd, bool wait_for_opc) {
    std::string out_cmd = cmd;  //+ "\r\n";
    logger(logDEBUG) << __PRETTY_FUNCTION__ << " Sending: " << out_cmd;
    // clear the status register bits
    m_com->send("*CLS");
    m_com->send(out_cmd);
    if (wait_for_opc) {
        waitForOPC();
    }
}

std::string KeysightDAQ970A::sendreceive(const std::string& cmd) {
    logger(logDEBUG) << __PRETTY_FUNCTION__ << " Sending: " << cmd;
    std::string out_cmd = cmd;  // + "\r\n";
    // clear the status register bits
    m_com->send("*CLS");
    std::string buf = m_com->sendreceive(out_cmd);
    // remove any whitespace characters at the end of the received message
    utils::rtrim(buf);
    return buf;
}

std::string KeysightDAQ970A::generateChannelListString(
    std::vector<unsigned> channels) {
    std::stringstream channelList;
    channelList << " (@";
    for (size_t i = 0; i < channels.size(); i++) {
        channelList << std::to_string(channels.at(i));
        if (i != channels.size() - 1) channelList << ",";
    }
    channelList << ")";
    return channelList.str();
}

std::vector<double> KeysightDAQ970A::samplesFromResponse(
    const std::string& response, unsigned n_expected) {
    std::vector<std::string> samples = utils::split(response, ",");

    if (samples.size() != n_expected) {
        std::stringstream e;
        e << "Sample string has unexpected number of samples (="
          << samples.size() << ") when only " << n_expected
          << " is/are expeced (sample response: " << response << ")";
        throw std::runtime_error(e.str());
    }

    std::transform(samples.begin(), samples.end(), samples.begin(),
                   [](std::string& s) {
                       utils::trim(s);
                       return s;
                   });

    std::vector<double> out(samples.size());
    std::transform(samples.begin(), samples.end(), out.begin(),
                   [](std::string& s) { return std::stod(s); });
    return out;
}

void KeysightDAQ970A::configDCV(const std::string& channelListString) {
    ScopeLock lock(m_com);
    // use defaults for most other settings
    send("CONF:VOLT:DC AUTO," + channelListString);
    send("SENS:VOLT:DC:NPLC 1," + channelListString);
}

void KeysightDAQ970A::configDCI(const std::string& channelListString) {
    ScopeLock lock(m_com);
    // use defaults for most other settings
    send("CONF:CURR:DC AUTO," + channelListString);
    send("SENS:CURR:DC:NPLC 1," + channelListString);
}

void KeysightDAQ970A::configRES(const std::string& channelListString,
                                bool use4w) {
    ScopeLock lock(m_com);
    // use defaults for most other settings
    std::string func = "RES";
    if (use4w) func = "FRES";
    send("CONF:" + func + channelListString);
}

void KeysightDAQ970A::configCAP(const std::string& channelListString) {
    ScopeLock lock(m_com);
    // use defaults for most other settings
    send("CONF:CAP AUTO," + channelListString);
}

bool KeysightDAQ970A::channelsAreIncreasingOrder(
    const std::vector<unsigned>& channels) {
    // c.f.
    // https://stackoverflow.com/questions/43219003/c-is-there-stl-algorithm-to-check-if-the-range-is-strictly-sorted
    auto iter = std::adjacent_find(channels.begin(), channels.end(),
                                   std::greater_equal<unsigned>());
    return iter == channels.end();
}

void KeysightDAQ970A::validateChannels(std::vector<unsigned> channels,
                                       KeysightDAQ970A::Function func) {
    // enforce that channels are monotonically increasing since this
    // is the order that the hardware returns the measurements in
    if (!channelsAreIncreasingOrder(channels)) {
        throw std::runtime_error(
            "KeysightDAQ970A::measure Provided channels must be given in "
            "monotonically increasing order");
    }

    // check measurement type specific things
    switch (func) {
        case KeysightDAQ970A::Function::VoltageDC:
        case KeysightDAQ970A::Function::Resistance:
        case KeysightDAQ970A::Function::Resistance4W:
        case KeysightDAQ970A::Function::Capacitance:
            break;
        case KeysightDAQ970A::Function::CurrentDC:
            bool channels_ok = true;
            for (unsigned channel : channels) {
                if (std::find(m_validChannelsDCI.begin(),
                              m_validChannelsDCI.end(),
                              channel) == m_validChannelsDCI.end()) {
                    channels_ok = false;
                    logger(logERROR) << "Invalid channel (=" << channel
                                     << ") for DC current measurement";
                }
            }
            if (!channels_ok) {
                throw std::runtime_error(
                    "Invalid channel configuration for DC current "
                    "measurement "
                    "with DACM901A");
            }
            break;
    }
}

void KeysightDAQ970A::clearStatMeasurements() {
    m_average_last.clear();
    m_stddev_last.clear();
    m_max_last.clear();
    m_min_last.clear();
}

std::vector<double> KeysightDAQ970A::measure(std::vector<unsigned> channels,
                                             KeysightDAQ970A::Function func) {
    // clear the status register bits
    m_com->send("*CLS");

    validateChannels(channels, func);
    std::string channelList = generateChannelListString(channels);

    ScopeLock lock(m_com);

    // configure the channels for the specific measurement
    std::string funcName = "";
    switch (func) {
        case KeysightDAQ970A::Function::VoltageDC:
            funcName = "VoltageDC";
            configDCV(channelList);
            break;
        case KeysightDAQ970A::Function::CurrentDC:
            funcName = "CurrentDC";
            configDCI(channelList);
            break;
        case KeysightDAQ970A::Function::Resistance:
            funcName = "Resistance";
            configRES(channelList, /*use4w*/ false);
            break;
        case KeysightDAQ970A::Function::Resistance4W:
            funcName = "Resistance4w";
            configRES(channelList, /*use4w*/ true);
            break;
        case KeysightDAQ970A::Function::Capacitance:
            funcName = "Capacitance";
            configCAP(channelList);
            break;
    }

    // clear stored data from previous measurements (if any)
    clearStatMeasurements();

    // initiate and trigger the measurement
    std::string received;
    bool do_stat = (m_trig_count > 1);
    if (!do_stat) {
        send("TRIG:COUNT 1");
        received = sendreceive("READ?");
    } else {
        send("CALC:AVER:CLE" + channelList);
        send("TRIG:COUNT " + std::to_string(m_trig_count));

        // Initiate the triggers. Do not wait for the OPC? query
        // as is usually done with the send calls, but poll
        // the event status register for event completion, as the
        // sending of many triggers may take some time and lead to
        // the OPC? queries timing out.
        send("INIT", false);
        waitForEventCompletion();

        // indices of statistics in returned data strings
        unsigned avg_index = 0;
        unsigned stddev_index = 1;
        unsigned min_index = 2;
        unsigned max_index = 3;

        // readout the statistics gathered across all the channels in the
        // channels list
        std::string stat_func = "ALL";
        switch (m_measurement_stat) {
            case Statistic::All:
                stat_func = "ALL";
                break;
            case Statistic::Average:
                stat_func = "AVER";
                break;
            case Statistic::StdDeviation:
                stat_func = "SDEV";
                break;
            case Statistic::Maximum:
                stat_func = "MAX";
                break;
            case Statistic::Minimum:
                stat_func = "MIN";
                break;
            case Statistic::PeakToPeak:
                stat_func = "PTP";
                break;
        }
        std::string aver_cmd = "CALC:AVER:" + stat_func + "?" + channelList;
        received = sendreceive(aver_cmd);

        if (m_measurement_stat == Statistic::All) {
            // iterate over all channels' statistics measurements, which
            // repeat in the order:
            // <average>,<stddev>,<min>,<max>,<trig-count>,<average>,<stddev>,<min>,<max>,<trig-count>,
            // ...
            std::vector<std::string> splits = utils::split(received, ",");
            unsigned n_quantities = splits.size();
            unsigned n_expected =
                5 *
                channels.size();  // there are 5 stats returned for each channel
            if (n_quantities != n_expected) {
                std::stringstream e;
                e << "Expected " << n_expected
                  << " quantities from statistics measurements (5 "
                     "statistics "
                     "for "
                  << channels.size() << " channels to measure), but received "
                  << n_quantities;
                throw std::runtime_error(e.str());
            }

            // create a string containing the quantities to be returned from
            // this function
            std::stringstream received_quantities;
            for (size_t ichan = 0; ichan < n_expected; ichan += 5) {
                // build the measurement string for the quantity that will
                // be returned from this function
                if (ichan > 0) {
                    received_quantities << ",";
                }
                received_quantities << splits.at(avg_index + ichan);

                // store all the statistics measurements
                m_average_last.push_back(
                    std::stod(splits.at(avg_index + ichan)));
                m_stddev_last.push_back(
                    std::stod(splits.at(stddev_index + ichan)));
                m_max_last.push_back(std::stod(splits.at(max_index + ichan)));
                m_min_last.push_back(std::stod(splits.at(min_index + ichan)));
            }
            received = received_quantities.str();
        }
    }

    std::vector<double> results(channels.size());
    try {
        results = samplesFromResponse(received, channels.size());
        switch (m_measurement_stat) {
            case Statistic::All:
                break;
            case Statistic::Average:
                m_average_last = results;
                break;
            case Statistic::StdDeviation:
                m_stddev_last = results;
                break;
            case Statistic::Maximum:
                m_max_last = results;
                break;
            case Statistic::Minimum:
                m_min_last = results;
                break;
            default:
                break;
        }
    } catch (std::exception& e) {
        logger(logERROR) << __PRETTY_FUNCTION__ << " Exception caught during "
                         << funcName << " measurement: " << e.what();
    }
    return results;
}

double KeysightDAQ970A::measureDCV(unsigned channel) {
    std::vector<double> measurements =
        measure({channel}, KeysightDAQ970A::Function::VoltageDC);
    return measurements.at(0);
}

std::vector<double> KeysightDAQ970A::measureDCV(
    const std::vector<unsigned>& channels) {
    return measure(channels, KeysightDAQ970A::Function::VoltageDC);
}

double KeysightDAQ970A::measureDCI(unsigned channel) {
    std::vector<double> measurements =
        measure({channel}, KeysightDAQ970A::Function::CurrentDC);
    return measurements.at(0);
}

std::vector<double> KeysightDAQ970A::measureDCI(
    const std::vector<unsigned>& channels) {
    return measure(channels, KeysightDAQ970A::Function::CurrentDC);
}

double KeysightDAQ970A::measureRES(unsigned channel, bool use4w) {
    KeysightDAQ970A::Function func =
        (use4w ? KeysightDAQ970A::Function::Resistance4W
               : KeysightDAQ970A::Function::Resistance);
    std::vector<double> measurements = measure({channel}, func);
    return measurements.at(0);
}

std::vector<double> KeysightDAQ970A::measureRES(
    const std::vector<unsigned>& channels, bool use4w) {
    KeysightDAQ970A::Function func =
        (use4w ? KeysightDAQ970A::Function::Resistance4W
               : KeysightDAQ970A::Function::Resistance);
    return measure(channels, func);
}

double KeysightDAQ970A::measureCAP(unsigned channel) {
    std::vector<double> measurements =
        measure({channel}, KeysightDAQ970A::Function::Capacitance);
    return measurements.at(0);
}

std::vector<double> KeysightDAQ970A::measureCAP(
    const std::vector<unsigned>& channels) {
    return measure(channels, KeysightDAQ970A::Function::Capacitance);
}
