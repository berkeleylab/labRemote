#include "MeterRegistry.h"

#include <iostream>

namespace EquipRegistry {
typedef ClassRegistry<IMeter, std::string> RegistryMeter;

static RegistryMeter& registry() {
    static RegistryMeter instance;
    return instance;
}

bool registerMeter(
    const std::string& model,
    std::function<std::shared_ptr<IMeter>(const std::string&)> f) {
    return registry().registerClass(model, f);
}

std::shared_ptr<IMeter> createMeter(const std::string& model,
                                    const std::string& name) {
    auto result = registry().makeClass(model, name);
    if (result == nullptr) {
        std::cout << "No meter (IMeter) of type " << model
                  << ", matching the name \"" << name << "\" found\n";
    }
    return result;
}

std::vector<std::string> listMeter() { return registry().listClasses(); }
}  // namespace EquipRegistry
