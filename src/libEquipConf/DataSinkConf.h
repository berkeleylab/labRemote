#ifndef DATASINKCONF__H
#define DATASINKCONF__H

#include <nlohmann/json.hpp>
#include <string>
#include <unordered_map>

#include "CombinedSink.h"
#include "IDataSink.h"

/** \brief Factory for discovering data sink resources and instantiate/configure
 * them.
 *
 * This class has Python bindings.
 *
 * Valid JSON config file contains the follow blocks:
 *  - `datasinks`: type of measurement format (i.e. ConsoleSink)
 *  - `datastreams`: instances of CombinedSink for streaming data to multiple
 * sinks
 *
 * The `datastreams` block is used to define combinations of sinks declared in
 * the `datasinks` block. The sub-sinks in a stream are specified as a list of
 * `datasink` names under the `sinks` key.
 *
 * The streams are implemented using the CombinedSink object that then forwards
 * the calls to sub-sinks.
 *
 * It is recommended that application use data streams for saving data as they
 * are much more flexible than individual sink. They allow saving data to
 * multiple and varied combinations of destinations.
 *
 * An example of the hardware configuration file content is:
 * ```json
 * "datasinks":{
 *   "Console":{
 *     "sinktype": "ConsoleSink"
 *   },
 *   "File":{
 *     "sinktype": "CSVSink",
 *     "directory": "myOutputData"
 *   },
 * },
 * "datastreams":{
 *   "TestData":{
 *     "sinks": ["Console", "File"]
 *   },
 *   "PowerSupplies":{
 *     "sinks": ["Console"]
 *   }
 * }
 * ```
 */
class DataSinkConf {
 public:
    /** \name Configuration
        @{ */

    //! \brief Constructor
    DataSinkConf();

    //! \brief Constructor
    /**
     * @param hardwareConfigFile input JSON file with list of datasink resources
     * and options
     */
    DataSinkConf(const std::string& hardwareConfigFile);

    //! \brief Constructor
    /**
     * @param hardwareConfig JSON object with list of datasink sources and
     * options
     */
    DataSinkConf(const nlohmann::json& hardwareConfig);

    ~DataSinkConf();

    //! \brief Set input hardware list file
    /**
     * @param hardwareConfigFile input JSON file with list of datasink resources
     * and options
     */
    void setHardwareConfig(const std::string& hardwareConfigFile);

    //! \brief Set input hardware list file
    /**
     * @param hardwareConfig input JSON object with list of datasink resources
     * and options
     */
    void setHardwareConfig(const nlohmann::json& hardwareConfig);

    //! \brief Get sink JSON configuration
    /**
     * @param label data sink name
     * @return data sink JSON configuration (by reference)
     */
    nlohmann::json getDataSinkConf(const std::string& label);

    //! \brief Get stream JSON configuration
    /**
     * @param label data stream name
     * @return data stream JSON configuration (by reference)
     */
    nlohmann::json getDataStreamConf(const std::string& label);

    /** @} */

 public:
    /** \name Get data sinks/streams (by label)
     * @{
     */

    //! \brief Get IDataSink object corresponding to name
    /**
     * If this is the first time the data sink name is requested:
     *  1. Create the object
     *
     * @param name label for the `datasink` object in JSON configuration file
     */
    std::shared_ptr<IDataSink> getDataSink(const std::string& name);

    //! \brief Get CombinedSink object corresponding to name
    /**
     * If this is the first time the data stream is requested:
     *  1. Create the object
     *  2. Get and associate sinks using DataSinkConf::getDataSink
     *
     * @param name label for the `datastream` object in JSON configuration file
     */
    std::shared_ptr<CombinedSink> getDataStream(const std::string& name);

    /** @} */

 private:
    /// JSON file with hardware list and options (see example
    /// input-hw.nlohmann::json file for syntax)
    nlohmann::json m_hardwareConfig;

    /// Stored handles of IDataSink pointers created
    std::unordered_map<std::string, std::shared_ptr<IDataSink>> m_dataSinks;

    /// Stored handles of CombinedSink pointers created
    std::unordered_map<std::string, std::shared_ptr<CombinedSink>>
        m_dataStreams;
};

#endif
