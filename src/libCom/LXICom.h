#ifndef LXICOM_H
#define LXICOM_H

#include <arpa/inet.h>  //inet_addr

#include "ICom.h"

/**
 * Implementation of communication with an LXI (LAN) device through TCP sockets.
 * Device locking is not supported!
 */

class LXICom : public ICom {
 public:
    /** \brief Create communication object and set settings
     * @param host Device hostname
     * @param port Device port for LXI communication
     *
     * Notice that the TCP communication does not support locking by itself.
     * Your particular device might accept only 1 TCP connection at a time.
     * That would seem like locking. But in principle devices can do whatever
     * they want. For example, the power supply that is used for development
     * testing, TTIPL303QMD-P, keeps 2 sockets on port 9221 for LXI commands. It
     * allows 2 TCP connections, and refuses others.
     *
     * This class connects only once, on `init()`.
     * And it re-establishes the connection if the device orderly closes it.
     * Other methods assume the connection is open.
     * If it is not so, TCP routines will timeout and throw exceptions.
     *
     * The TCP routines are exposed for user's convenience.
     */
    LXICom(const std::string& host = "TTIPL303QMD-P",
           const uint16_t port = 9221);

    ~LXICom();

    /** Configure the device address and establish connection
     */
    virtual void init();

    /** \brief Configure on JSON object
     *
     * Valid keys:
     *  - `host`: the hostname or ip address
     *  - `port`: the port number
     *
     * \param config JSON configuration
     */
    virtual void setConfiguration(const nlohmann::json& config);

    /** Send data to device
     *
     * Throw `std::runtime_error` on error.
     *
     * \param buf Data to be sent
     * \param length Number of characters in `buf` that should be sent
     */
    virtual void send(char* buf, size_t length);

    /** Send data to device
     *
     * Throw `std::runtime_error` on error.
     *
     * \param buf Data to be sent
     */
    virtual void send(const std::string& buf);

    /** Read data from device
     *
     * Throw `std::runtime_error` on error.
     *
     * \return Received data
     */
    virtual std::string receive();

    /** Read data from device
     *
     * Throw `std::runtime_error` on error.
     *
     * \param buf Buffer where to store results
     * \param length Number of characters to receive.
     *
     * \return Number of characters received
     */
    virtual uint32_t receive(char* buf, size_t length);

    /** Write data to device and receive reply
     *
     * Throw `std::runtime_error` on error.
     *
     * \param cmd Data to be sent
     *
     * \return Returned data
     */
    virtual std::string sendreceive(const std::string& cmd);

    /** Write data to device and receive reply
     *
     * Throw `std::runtime_error` on error.
     *
     * \param wbuf Data to be sent
     * \param wlength Number of characters in `wbuf` that should be sent
     * \param rbuf Buffer where to store results
     * \param rlength Number of characters to receive.
     *
     * \return Number of characters received
     */
    virtual void sendreceive(char* wbuf, size_t wlength, char* rbuf,
                             size_t rlength);

    /** Locking is not supported!
     *
     * Logs a WARNING.
     */
    virtual void lock();

    /** Locking is not supported!
     *
     * Logs a WARNING.
     */
    virtual void unlock();

    /** TCP communication routines @{ */

    //! Open a TCP connection to the device
    int tcp_connect(void);
    //! Close the TCP connection
    int tcp_disconnect(void);
    //! Send a message to the open TCP connection
    int tcp_send(const char* message);
    //! Receive a response from the open TCP connection
    int tcp_recv(std::string& response);

 private:
    /** Configuration @{ */

    //! Full address of the device LXI facility
    //! hostname or ip address
    std::string m_host;
    //! LXI port number (9221)
    uint16_t m_port;

    //! Parsed address of the device
    struct sockaddr_in m_server_addr;
    //! Descriptor of an open socket connection to the device
    int m_socket_desc;

    /** @} */

    /** Run-time variables @{ */

    //! Temporary buffer for received messages
    std::string m_receive_buf;
};

#endif
