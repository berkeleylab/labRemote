#include "TextSerialCom.h"

#include <fcntl.h>
#include <termios.h>
#include <unistd.h>

#include <cerrno>
#include <cstring>
#include <stdexcept>

#include "Logger.h"
#include "ScopeLock.h"

// Register com
#include "ComRegistry.h"
REGISTER_COM(TextSerialCom)

TextSerialCom::TextSerialCom(const std::string& port, SerialCom::BaudRate baud,
                             bool parityBit, bool twoStopBits, bool flowControl,
                             SerialCom::CharSize charsize)
    : SerialCom(port, baud, parityBit, twoStopBits, flowControl, charsize) {}

TextSerialCom::TextSerialCom() : SerialCom() {}

void TextSerialCom::setTermination(const std::string& termination,
                                   const std::string& returnTermination) {
    m_termination = termination;
    if (returnTermination.empty()) {
        m_returnTermination = m_termination;
    } else {
        m_returnTermination = returnTermination;
    }
}

std::string TextSerialCom::termination() const { return m_termination; }

std::string TextSerialCom::returnTermination() const {
    return m_returnTermination;
}

void TextSerialCom::setConfiguration(const nlohmann::json& config) {
    SerialCom::setConfiguration(config);

    bool return_termination_specified{false};

    for (const auto& kv : config.items()) {
        if (kv.key() == "termination") {
            m_termination = kv.value();
        } else if (kv.key() == "returnTermination") {
            m_returnTermination = kv.value();
            return_termination_specified = true;
        }
    }

    if (!return_termination_specified) {
        m_returnTermination = m_termination;
    }
}

void TextSerialCom::send(const std::string& buf) {
    logger(logDEBUG2) << __PRETTY_FUNCTION__ << " -> Sending: " << buf;
    SerialCom::send(buf + m_termination);
}

void TextSerialCom::send(char* buf, size_t length) {
    send(std::string(buf, length));
}

std::string TextSerialCom::receive() {
    ScopeLock lock(this);

    std::string buf;
    std::string newbuf;
    do {
        newbuf = SerialCom::receive();
        if (newbuf.size() == 0)
            throw std::runtime_error(
                "TextSerialCom: Read timeout reached without seeing "
                "termination.");
        buf += newbuf;
    } while (buf.size() < m_returnTermination.size() ||
             buf.substr(buf.size() - m_returnTermination.size()) !=
                 m_returnTermination);

    // rstrip the returnTermination
    buf = buf.substr(0, buf.size() - m_returnTermination.size());

    logger(logDEBUG2) << __PRETTY_FUNCTION__ << " -> Received: " << buf;

    return buf;
}

uint32_t TextSerialCom::receive(char* buf, size_t length) {
    ScopeLock lock(this);

    uint32_t n_read = 0;
    uint32_t tmp_n_read = 0;
    do {
        tmp_n_read = SerialCom::receive(buf + n_read, length - n_read);
        if (tmp_n_read == 0)
            throw std::runtime_error(
                "TextSerialCom: Read timeout reached without seeing "
                "termination.");
        n_read += tmp_n_read;
    } while (n_read < m_returnTermination.size() ||
             strncmp(buf + n_read - m_returnTermination.size(),
                     m_returnTermination.c_str(),
                     m_returnTermination.size()) != 0);

    // rstrip new lines from end
    n_read -= m_returnTermination.size();

    return n_read;
}
