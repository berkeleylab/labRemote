#include "AgilentE3631APs.h"

// Register power supply
#include "PowerSupplyRegistry.h"
REGISTER_POWERSUPPLY(AgilentE3631APs)

AgilentE3631APs::AgilentE3631APs(const std::string& name)
    : AgilentPs(name, {"E3631A"}, 3) {}

void AgilentE3631APs::turnOn(unsigned channel) { SCPIPs::turnOn(channel); }

void AgilentE3631APs::turnOff(unsigned channel) { SCPIPs::turnOff(channel); }
