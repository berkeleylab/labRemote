#ifndef RS_NGP804_H
#define RS_NGP804_H

#include <chrono>
#include <memory>
#include <string>

#include "SCPIPs.h"
#include "SerialCom.h"

/** \brief ROHDE&SCHWARZ NGP804
 * Implementation for the ROHDE&SCHWARZ NGP804 power supply.
 *
 * [Programming
 * Manual](https://scdn.rohde-schwarz.com/ur/pws/dl_downloads/pdm/cl_manuals/user_manual/5601_5610_01/NGP800_User_Manual_en_11.pdf)
 *
 */
class RS_NGP804 : public SCPIPs {
 public:
    RS_NGP804(const std::string& name);
    ~RS_NGP804() = default;
};

#endif
