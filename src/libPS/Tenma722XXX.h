#ifndef TENMA722XXX_H
#define TENMA722XXX_H

#include <chrono>
#include <iomanip>
#include <memory>
#include <sstream>
#include <string>

#include "IPowerSupply.h"

/** \brief Tenma 72-2930 single channel power supply
 *
 * Implementation for the Tenma 72-2535, 72-2540*, 72-2545*, 72-2550*, 72-2930
 * and 72-2940* single channel power supplies
 *
 * * The driver has not been tested on the models with a *. However, the two
 * models that has been tested, seems like are built from the same template, so
 * there is a very high probability that the generic driver will work with those
 * as well.
 *
 * Taken from
 * [here](https://pyexplabsys.readthedocs.io/_modules/PyExpLabSys/drivers/tenma.html#TenmaBase),
 * [here](https://github.com/kxtells/tenma-serial/blob/master/tenma/tenmaDcLib.py),
 * [some
 * manuals](https://www.element14.com/community/docs/DOC-75108/l/protocol-information-for-tenma-72-2550-and-tenma-72-2535-qa-window-driver)
 * and [datasheet](http://www.farnell.com/datasheets/2372012.pdf).
 *
 */
class Tenma722XXX : public IPowerSupply {
 public:
    Tenma722XXX(const std::string& name);
    ~Tenma722XXX() = default;

    /** \name Communication
     * @{
     */

    virtual bool ping();

    virtual std::string identify();

    /** @} */

    /** \name Power Supply Control
     * @{
     */

    virtual void reset();
    virtual void turnOn(unsigned channel);
    virtual void turnOff(unsigned channel);
    virtual bool isOn(unsigned channel);
    virtual void beepOn();
    virtual void beepOff();
    /** Save the panel setting in memory slot, only works on the active slot
     * \param Memory slot number (1 to 5)
     */
    virtual void saveSetting(unsigned mem);
    /** Recall saved setting in memory slot
     * \param Memory slot number (1 to 5)
     */
    virtual void recallSetting(unsigned mem);
    /** @} */

    /** \name Current Control and Measurement
     * @{
     */

    virtual void setCurrentLevel(double cur, unsigned channel = 1);
    virtual double getCurrentLevel(unsigned channel = 1);
    //! setCurrentProtect toggles OCP on or off, maxcur does not have any
    //! effect; to set current compliance, use setCurrentLevel
    virtual void setCurrentProtect(double maxcur = 0, unsigned channel = 1);
    virtual double measureCurrent(unsigned channel = 1);

    /** @} */

    /** \name Voltage Control and Measurement
     * @{
     */

    virtual void setVoltageLevel(double volt, unsigned channel = 1);
    virtual double getVoltageLevel(unsigned channel = 1);
    //! setVoltageProtect toggles OVP on or off, maxvolt does not have any
    //! effect; to set voltage compliance, use setVoltageLevel
    virtual void setVoltageProtect(double maxvolt = 0, unsigned channel = 1);
    virtual double measureVoltage(unsigned channel = 1);

    /** @} */

 private:
    /**
     * Convert number to scientific notation
     *
     * \param a_value number to convert
     * \param n number of digitcs after the decimal
     *
     * \return x.xxxxxxEyy
     */
    template <typename T>
    std::string to_string_with_precision(const T a_value, const int n = 3) {
        std::ostringstream out;
        out << std::setprecision(n) << a_value;
        return out.str();
    }
};

#endif
