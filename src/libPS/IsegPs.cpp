#include "IsegPs.h"

#include <algorithm>
#include <chrono>
#include <cmath>
#include <thread>

#include "Logger.h"
#include "ScopeLock.h"
#include "StringUtils.h"
#include "TextSerialCom.h"

// Register power supply
#include "PowerSupplyRegistry.h"
REGISTER_POWERSUPPLY(IsegPs)

IsegPs::IsegPs(const std::string& name, std::vector<std::string> models,
               unsigned maxChannels)
    : IPowerSupply(name, models), m_maxChannels(maxChannels) {}

bool IsegPs::ping() {
    std::string result = sendreceive("*IDN?");
    return !result.empty();
}

void IsegPs::reset() {
    send("*RST");
    if (!ping()) throw std::runtime_error("No communication after reset.");
}

std::string IsegPs::identify() {
    std::string idn = sendreceive("*IDN?");
    return idn;
}

void IsegPs::turnOn(unsigned channel) { send(":VOLT ON,", channel); }

void IsegPs::turnOff(unsigned channel) { send(":VOLT OFF,", channel); }

void IsegPs::setCurrentLevel(double cur, unsigned channel) {
    send(":CURR " + std::to_string(cur) + ",", channel);
}

double IsegPs::getCurrentLevel(unsigned channel) {
    std::string const recv = sendreceive(":READ:CURR:NOM?", channel);
    return std::stod(utils::trim_last_char(recv));  // remove the A from the end
}

void IsegPs::setCurrentProtect(double maxcur, unsigned channel) {
    logger(logWARNING) << "setCurrentProtect() not implemented for this PS. "
                          "Manual adjustment at PS required.";
}

double IsegPs::getCurrentProtect(unsigned channel) {
    std::string const recv = sendreceive(":READ:CURR:LIM?", channel);
    return std::stod(utils::trim_last_char(recv));
}

double IsegPs::measureCurrent(unsigned channel) {
    std::string const recv = sendreceive("MEAS:CURR?", channel);
    return std::stod(utils::trim_last_char(recv));  // remove the A from the end
}

void IsegPs::setVoltageLevel(double volt, unsigned channel) {
    auto channelOn = isOn(channel);
    auto PSPolarity = getPolarity(channel);  // the current polarity set
    auto ReqPolarityPos =
        !std::signbit(volt);  // is requested volt not negative
    if (!ReqPolarityPos && (PSPolarity == IPowerSupply::Polarity::Positive)) {
        if (channelOn) {
            throw IPowerSupply::polarity_error(
                "Attempting to switch to negative from positive polarity while "
                "channel is on");
        } else {
            setPolarity(IPowerSupply::Polarity::Negative, channel);
            // need to wait before sending the new voltage, if no wait, the
            // voltage will not be set correctly
            std::this_thread::sleep_for(std::chrono::seconds(1));
        }
    } else if (ReqPolarityPos &&
               (PSPolarity == IPowerSupply::Polarity::Negative)) {
        if (channelOn) {
            throw IPowerSupply::polarity_error(
                "Attempting to switch to positive from negative polarity while "
                "channel is on");
        } else {
            setPolarity(IPowerSupply::Polarity::Positive, channel);
            // need to wait before sending the new voltage, if no wait, the
            // voltage will not be set correctly
            std::this_thread::sleep_for(std::chrono::seconds(1));
        }
    }
    send(":VOLTAGE " + std::to_string(volt) + ",", channel);
}

double IsegPs::getVoltageLevel(unsigned channel) {
    std::string const recv = sendreceive(":READ:VOLTAGE?", channel);
    return std::stod(utils::trim_last_char(recv));  // remove the V from the end
}

void IsegPs::setVoltageProtect(double maxvolt, unsigned channel) {
    logger(logWARNING) << "setVoltageProtect() not implemented for this PS. "
                          "Manual adjustment at PS required.";
}

double IsegPs::getVoltageProtect(unsigned channel) {
    std::string const recv = sendreceive(":READ:VOLTAGE:LIM?", channel);
    return std::stod(utils::trim_last_char(recv));  // remove the V from the end
}

double IsegPs::measureVoltage(unsigned channel) {
    std::string const recv = sendreceive(":MEAS:VOLT?", channel);
    return std::stod(utils::trim_last_char(recv));  // remove the V from the end
}

void IsegPs::send(const std::string& cmd) {
    std::string opcreply = sendreceive(cmd + ";*OPC?");
    utils::rtrim(opcreply);
    if (opcreply != "1")
        throw std::runtime_error("IsegPs::send: *OPC? check failed");
}

void IsegPs::send(const std::string& cmd, unsigned channel) {
    if (m_maxChannels > 0) {  // In-range channel check
        if (channel > m_maxChannels)
            throw std::runtime_error("Invalid channel: " +
                                     std::to_string(channel));
    }
    std::string retCmd = cmd;
    if (m_maxChannels != 1) retCmd += " (@" + std::to_string(channel) + ")";
    send(retCmd);
}

IPowerSupply::Polarity IsegPs::getPolarity(unsigned channel) {
    if (m_maxChannels > 0) {  // In-range channel check
        if (channel > m_maxChannels)
            throw std::runtime_error("Invalid channel: " +
                                     std::to_string(channel));
    }
    std::string cmd = ":CONF:OUTP:POL? (@" + std::to_string(channel) + ")";
    std::string recv = sendreceive(cmd);
    if (recv == "p")
        return IPowerSupply::Polarity::Positive;
    else if (recv == "n")
        return IPowerSupply::Polarity::Negative;
    else
        throw std::runtime_error(
            "IsegPs::getPolarityPos: could not determine polarity");
}

void IsegPs::setPolarity(IPowerSupply::Polarity polarity, unsigned channel) {
    if (m_maxChannels > 0) {  // In-range channel check
        if (channel > m_maxChannels)
            throw std::runtime_error("Invalid channel: " +
                                     std::to_string(channel));
    }
    turnOff(channel);
    std::string cmd;
    if (polarity == IPowerSupply::Polarity::Positive)
        cmd = ":CONF:OUTP:POL p, (@" + std::to_string(channel) + ")";
    else if (polarity == IPowerSupply::Polarity::Negative)
        cmd = ":CONF:OUTP:POL n, (@" + std::to_string(channel) + ")";
    else
        throw std::runtime_error("Invalid polarity");
    send(cmd);
}

bool IsegPs::isOn(unsigned channel) {
    if (m_maxChannels > 0) {  // In-range channel check
        if (channel > m_maxChannels)
            throw std::runtime_error("Invalid channel: " +
                                     std::to_string(channel));
    }
    std::string const cmd = ":READ:VOLT:ON? (@" + std::to_string(channel) + ")";
    auto recv = sendreceive(cmd);
    if (recv == "1")
        return true;
    else if (recv == "0")
        return false;
    else
        throw std::runtime_error(
            "IsegPs::isOn: could not determine if channel is on");
}

std::string IsegPs::sendreceive(const std::string& cmd) {
    ScopeLock lock(m_com);
    auto rec = m_com->sendreceive(cmd);
    // In case the IsegSHR20xxPs uses a serial communication via USB/Serial its
    // responses are echoed to validate communication integrity in these cases,
    // the result looks like: <CMD><Term><RES><Term>, the ultimate termination
    // is already stripped by the Com implementation, here we need to split the
    // <CMD> and <RES> and validate that the read back <CMD> is the one we sent
    auto text_serial_port = dynamic_cast<TextSerialCom*>(m_com.get());
    if (text_serial_port) {
        auto term_seq = text_serial_port->returnTermination();
        auto rec_split = utils::split(rec, term_seq);
        if (cmd != rec_split.at(0))
            throw std::runtime_error(
                "IsegPs::sendreceive: readback of command failed!");
        if (rec_split.size() != 2)
            throw std::runtime_error(
                "IsegPs::sendreceive: received invalid reaback!");
        return rec_split.at(1);
    } else {
        return rec;
    }
}

std::string IsegPs::sendreceive(const std::string& cmd, unsigned channel) {
    if (m_maxChannels > 0) {  // In-range channel check
        if (channel > m_maxChannels)
            throw std::runtime_error("Invalid channel: " +
                                     std::to_string(channel));
    }
    std::string retCmd = cmd;
    if (m_maxChannels != 1) retCmd += " (@" + std::to_string(channel) + ")";

    return sendreceive(retCmd);
}
