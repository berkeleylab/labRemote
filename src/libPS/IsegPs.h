#ifndef ISEGPS_H
#define ISEGPS_H

#include <chrono>
#include <memory>
#include <string>

#include "IPowerSupply.h"

/**
 * Base implemetation for iseg power supplies that share
 * a very similar command set.
 *
 * Options are possible to add additional for error checking on
 *  - channel checking (maximum number of channels, 0 means no check is
 * performed)
 */
class IsegPs : public IPowerSupply {
 public:
    /**
     * @param name Name of the power supply
     * @param models List of supported models (empty means no check is
     * performed)
     * @param maxChannels Maximum number of channels in the power supply (0
     * means no maximum)
     */
    IsegPs(const std::string& name, std::vector<std::string> models = {},
           unsigned maxChannels = 0);
    ~IsegPs() = default;

    /** \name Communication
     * @{
     */

    virtual bool ping();

    /** @} */

    /** \name Power Supply Control
     * @{

    /**
     * Returns the model identifier of the PS connected
     *
     */
    virtual std::string identify();

    virtual void reset();

    /** Turn on specific channel
     *
     * @param channel the channel to turn on
     */
    virtual void turnOn(unsigned channel);

    /** Turn off specific channel
     *
     * @param channel the channel to turn off
     */
    virtual void turnOff(unsigned channel);

    /**
     * Returns true if the channel's output is on, and false otherwise
     *
     * @param channel the channel to query on state
     */
    virtual bool isOn(unsigned channel);

    /** @} */

    /** \name Current Control and Measurement
     * @{
     */

    virtual void setCurrentLevel(double cur, unsigned channel = 0);
    virtual double getCurrentLevel(unsigned channel = 0);
    virtual void setCurrentProtect(double maxcur, unsigned channel = 0);
    virtual double getCurrentProtect(unsigned channel = 0);
    virtual double measureCurrent(unsigned channel = 0);

    /** @} */

    /** \name Voltage Control and Measurement
     * @{
     */

    /**
     * Will set the voltage to the requested value for a given channel. Will
     * throw an exception if the channel is powered on and a polarity switch is
     * requested. Takes the signedness of the passed argument in order to
     * determine the polarity, i.e. -0 and 0 produce a different polarity
     *
     * @param volt the voltage to set
     * @param channel the channel to set the voltage
     */
    virtual void setVoltageLevel(double volt, unsigned channel = 0);
    virtual double getVoltageLevel(unsigned channel = 0);
    virtual void setVoltageProtect(double maxvolt, unsigned channel = 0);
    virtual double getVoltageProtect(unsigned channel = 0);
    virtual double measureVoltage(unsigned channel = 0);

    /** @} */

    /** \name Polarity Control and Measurement
     * @{
     */

    virtual IPowerSupply::Polarity getPolarity(unsigned channel = 0);
    virtual void setPolarity(IPowerSupply::Polarity polarity,
                             unsigned channel = 0);

    /** @} */

 protected:
    //\brief Send power supply command and wait for operation complete.
    /**
     * Sends `cmd` to the power supply followed up `*OPC?`. Then blocks until
     * a response is received. This prevents the program from terminating
     * before the program is processed.
     *
     * An error is thrown if no response to `*OPC?` is seen.
     *
     * \param cmd Power supply command to transmit.
     */
    void send(const std::string& cmd);

    //! \brief Add channel set to power supply send
    /**
     * Adds `" (@"+channel+")"` to the end of the actual command. This is
     * done without injecting `*OPC?`.
     *
     * The rest is achieved using `IsegPs::send(constd std::string& cmd)`.
     *
     * \param cmd Power supply command to transmit.
     * \param channel Target channel
     */
    void send(const std::string& cmd, unsigned channel);

    //\brief Send power supply command and return response.
    /**
     * Wrapper around `ICom::sendreceive`.
     *
     * \param cmd Power supply command to transmit.
     *
     * \return Parameter response.
     */
    std::string sendreceive(const std::string& cmd);

    //! \brief Add channel set to power supply sendreceive
    /**
     * Adds `" (@"+channel+")"` to the end of the actual command. This is
     * done without injecting `*OPC?`.
     *
     * The rest is achieved using `IsegPs::sendreceive(constd std::string&
     * cmd)`.
     *
     * \param cmd Power supply command to transmit.
     * \param channel Target channel
     *
     * \return Response for `cmd`
     */
    std::string sendreceive(const std::string& cmd, unsigned channel);

 private:
    /** PS limitations @{ */

    //! Maximum number of channels, 0 means unlimited
    uint32_t m_maxChannels = 0;
};

#endif
