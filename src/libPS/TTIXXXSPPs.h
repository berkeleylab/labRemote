#ifndef TTIXXXSPPS_H
#define TTIXXXSPPS_H

#include <chrono>
#include <memory>
#include <string>

#include "TTIPs.h"

/** \brief TTi XXXDP
 *
 * Implementation for the TTi dual channel power supplies
 * such as PL330DP.
 *
 */
class TTIXXXSPPs : public TTIPs {
 public:
    TTIXXXSPPs(const std::string& name);
    ~TTIXXXSPPs() = default;
};

#endif
