#include <dirent.h>
#include <sys/stat.h>

#include <chrono>
#include <cmath>
#include <fstream>
#include <iostream>
#include <memory>

#ifdef FTDI
#include "FT232H.h"
#include "I2CFTDICom.h"
#endif
#include "HIH6130.h"
#include "Logger.h"

#define NUMCHAR(X) (uint)((unsigned char)(X))

int main(int argc, char* argv[]) {
#ifndef FTDI
    logger(logERROR) << "FTDI support not enabled.";
    return -1;
#else
    // Initialize I2C (for DAC comm)
    std::shared_ptr<FT232H> ft232(new FT232H(MPSSEChip::Protocol::I2C,
                                             MPSSEChip::Speed::ONE_HUNDRED_KHZ,
                                             MPSSEChip::Endianness::MSBFirst));
    std::shared_ptr<I2CCom> i2c_com(new I2CFTDICom(ft232, 0x27));

    HIH6130 tempsensor(i2c_com);

    // Loop and measure
    while (true) {
        // Perform the measurement
        std::time_t time = std::time(nullptr);

        tempsensor.read();

        // Save the data
        logger(logINFO) << time << "\t" << tempsensor.status() << "\t"
                        << tempsensor.humidity() << "\t"
                        << tempsensor.temperature();
    }

#endif

    return 0;
}
