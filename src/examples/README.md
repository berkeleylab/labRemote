# Examples 

Tabulated below are the `labRemote` examples, with some additional details related to them.

| Example Name | Has Python Version? | `labRemote` libraries Used | Devices Used | Communication Used |
| -- | -- | -- | -- | -- |
| datasink_example | [Yes](src/examples/datasink_example.py) | `libDataSink` | | |
| [control_powersupply_example](#power-supply-control_powersupply_example) | [Yes](src/examples/control_powersupply_example.py) | `libDevCom`, `libPS`, `libEquipConf` | |
| ntc_example | [Yes](src/examples/ntc_example.py) | `libCom`, `libDevCom` | [NTCSensor](src/libDevCom/NTCSensor.h) | [TextSerialCom](src/libCom/TextSerialCom.h) |
| sht85_example | [Yes](src/examples/sht85_example.py) | `libDevCom`, `libCom` | [FT232H](src/libDevCom/FT232H.h), [SHT85](src/libDevCom/SHT85.h) | [I2CDevComuino](src/libCom/I2CDevComuino.h) |
| ft232h_adc_example | [Yes](src/examples/ft232h_adc_example.py) | `libDevCom` | [AD7998](src/libDevCom/AD799X.h), [FT232H](src/libDevCom/FT232H.h), [I2CFTDICom](src/libDevCom/I2CFTDICom) |
| tempsensor_monitor_example | [Yes](src/examples/tempsensor_monitor_example.py) | `libDevCom` | [FT232H](src/libDevCom/FT232H.h), [HIH61300](src/libDevCom/HIH6130.h), [I2CFTDICom](src/libDevCom/I2CFTDICom.h) | |
| temp_hum_monitor_example | [Yes](src/examples/temp_hum_monitor_example.py) | `libDevCom`, `libDataSink`, `libCom` | [NTCSensor](src/libDevCom/NTCSensor.h), [PtSensor](src/libDevCom/PtSensor.h), [HIH4000](src/libDevCom/HIH4000.h), [ADCDevComuino](src/libDevCom/ADCDevComuino.h) | [TextSerialCom](src/libCom/TextSerialCom.h)|
| [devcomuino_example](#arduino-communication) | [Yes](src/examples/devcomuino_example.py) | `libCom` | | [TextSerialCom](src/libCom/TextSerialCom) |
| Si7021_example | [Yes](src/examples/Si7021_example.py) | `libDevCom` | [Si7021](src/libDevCom/Si7021.h), [I2CDevComuino](src/libDevCom/I2CDevComuino.h) | |
| fluke_example | No | `libMeter` | [Fluke8842](src/libMeter/Fluke8842.h) | |
| scope_example | No | `libScope` | [PS6000](src/libScope/PS6000.h) | |
| pixel_scope_run | No | `libScope` | [PS6000](src/libScope/PS6000.h) | |




## Arduino Communication
Communication with an an Arduino-compatible micro-controller is enabled by flashing the [devcomuino.ino](arduino/devcomuino/devcomuino.ino)
firmware onto the micro-controller, after which it will respond to specific commands sent to it over its serial communication (established
via the USB serial connection with the device).

The [devcomuino_example](src/examples/devcomuino_example.py) illustrates how to send commands and receive responses from
a micro-controller running the [devcomuino.ino](arduino/devcomuino/devcomuino.ino) firmware using the [TextSerialCom](src/libCom/TextSerialCom.h) class.

## Power supply: `control_powersupply_example`
To run it, use:
```shell
$ ./examples/control_powersupply_example ./src/configs/input-hw.json low-voltage
```

Further information about its use:
```shell
$ ./examples/control_powersupply_example
[ERROR]   : Missing positional arguments.
Usage: ./examples/control_powersupply_example configfile.json channelname

List of options:
 -d, --debug       Enable more verbose printout, use multiple for increased debug level

```

This simple script shows how to create the hardware database and how to configure one of the channels listed (low-voltage). Then it programs the channel using the settings in the program block of the json file (this is optional), it powers the channel on, returns the measured voltage and current and powers it off.

To access functions that are only defined for a specific power supply, one needs to cast the pointer to the IPowerSupply object back to
the specific class. For example, beepOff() is a function that is implemented only for AgilentPS.  

The advantage of using the logical channel (PowerSupplyChannel) is that the same executable will work regardless of the power supply being 
multi- or single-channel. The only change must be in the json file.

