from labRemote import devcom, com

from argparse import ArgumentParser
import sys, inspect
from datetime import datetime
import logging
logging.basicConfig()
logger = logging.getLogger("sht85_example")

def sht85_example(use_ftdi, device, baud) :

    if use_ftdi :
        ##
        ## configure and initialize the FT232H device
        mpsse = devcom.MPSSEChip
        protocol = mpsse.Protocol.I2C
        speed = mpsse.Speed.ONE_HUNDRED_KHZ
        endianness = mpsse.Endianness.MSBFirst
        ft232 = devcom.FT232H(protocol, speed, endianness, "", "")

        ##
        ## i2c line and address for the sensor
        ##
        i2c = devcom.I2CFTDICom(ft232, 0x44)
    else :
        ##
        ## initialize the communication line
        ##
        try :
            baud_rate = getattr(com.SerialCom.BaudRate, 'Baud{0:d}'.format(baud))
            serial = com.TextSerialCom(device, baud_rate)
        except :
            logger.error(f"Unable to initialize serial communication with the device at port \"{device}\"")
            sys.exit(1)
        if serial is None :
            logger.error("Returned communication device is None")
            sys.exit(1)
        serial.setTermination("\r\n")
        serial.init()

        ##
        ## initialize the I2C bus line
        ##
        i2c = devcom.I2CDevComuino(0x44, serial)

    ##
    ## initialize the sensor on the I2C bus
    ##
    sensor = devcom.SHT85(i2c)

    if sensor is None :
        logger.error("Returned sensor device is None")
        sys.exit(1)

    ##
    ## perform continuous measurements
    ##
    while True :
        time.sleep(1)
        try :
            sensor.read()
        except RuntimeError as err :
            logger.error(f"Sensor read error: {err}, continuing...")
            continue
        now = datetime.now()
        logger.info(f"Measurement[{now}]: temperature = {sensor.temperature()}, humidity = {sensor.humidity()}")

if __name__ == "__main__" :

    parser = ArgumentParser(description = "Example of communicating with an I2C-based SHT85 device")
    parser.add_argument("--ftdi", action = "store_true", default = False,
        help = "Use FTDI-based FT232H device"
    )
    parser.add_argument("-p", "--port", default = "", type = str,
        help = "Port at which device is connected (e.g. /dev/ttyACM0) (leave blank if you wish to use FTDI-based FT232H device)"
    )
    parser.add_argument("-b", "--baud", default = 9600, type = int,
        help = "Baud rate for serial communication with device (ignore if you wish to use FTDI-based FT232H device)"
    )
    parser.parse_args()

    ##
    ## first check that FTDI support has been enabled in the current labRemote build
    ##
    if args.ftdi and not inspect.isclas(devcom.I2CFTDICom) :
        logger.error("Python support for FTDI devices has not been added!")
        sys.exit()
    elif not args.ftdi and args.port == "" :
        logger.error("You must provide a device port if not using FTDI-based device")
        sys.exit(1)
    sht85_example(args.ftdi, args.port, args.baud)
