#include "Julabo.h"

#include "ChillerRegistry.h"
REGISTER_CHILLER(Julabo)

Julabo::Julabo(const std::string& name) : IChiller(name, {"Julabo"}) {}

void Julabo::init() {}

void Julabo::turnOn() { m_com->send("out_mode_05 1"); }

void Julabo::turnOff() { m_com->send("out_mode_05 0"); }

void Julabo::setTargetTemperature(float temp) {
    std::string cmd = "out_sp_00";
    std::stringstream ss;
    ss << temp;
    cmd.append(" " + ss.str());
    m_com->send(cmd);
}

float Julabo::getTargetTemperature() {
    std::string response = m_com->sendreceive("in_sp_00");
    float temp;
    if (response.substr(0, 1) == "-")
        temp = -std::stof(response.substr(1));
    else
        temp = std::stof(response);
    return temp;
}

float Julabo::measureTemperature() {
    std::string response = m_com->sendreceive("in_pv_00");
    float temp;
    if (response.substr(0, 1) == "-")
        temp = -std::stof(response.substr(1));
    else
        temp = std::stof(response);
    return temp;
}

bool Julabo::getStatus() {
    std::string response = m_com->sendreceive("status");
    // Return codes:
    // 01 MANUAL START --> chiller is on and can be operated manually
    // 02 MANUAL STOP --> chiller is off and can be operated manually
    // 03 REMOTE START --> chiller is on and can be operated remotely
    // 04 REMOTE STOP --> chiller is off and can be operated remotely
    if (response.find("START") != std::string::npos)
        return true;
    else if (response.find("STOP") != std::string::npos)
        return false;
    else {
        logger(logDEBUG) << __PRETTY_FUNCTION__
                         << " -> Unexpected response: " << response;
        throw std::runtime_error("Unexpected response from chiller");
        return false;
    }
}
