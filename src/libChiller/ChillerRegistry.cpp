#include "ChillerRegistry.h"

#include <iostream>

namespace EquipRegistry {
typedef ClassRegistry<IChiller, std::string> RegistryChiller;

static RegistryChiller& registry() {
    static RegistryChiller instance;
    return instance;
}

bool registerChiller(
    const std::string& model,
    std::function<std::shared_ptr<IChiller>(const std::string&)> f) {
    return registry().registerClass(model, f);
}

std::shared_ptr<IChiller> createChiller(const std::string& model,
                                        const std::string& name) {
    auto result = registry().makeClass(model, name);
    if (result == nullptr) {
        std::cout << "No chiller (IChiller) of type " << model
                  << ", matching the name \"" << name << "\" found\n";
    }
    return result;
}

std::vector<std::string> listChiller() { return registry().listClasses(); }

}  // namespace EquipRegistry
