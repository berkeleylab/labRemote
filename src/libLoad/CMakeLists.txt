add_library(Load SHARED)
target_sources(Load
  PRIVATE
  Bk85xx.cpp
  TTILD400P.cpp
  )
target_link_libraries(Load PRIVATE Com Utils)
target_include_directories(Load PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})

set_target_properties(Load PROPERTIES VERSION ${labRemote_VERSION_MAJOR}.${labRemote_VERSION_MINOR})
install(TARGETS Load)
